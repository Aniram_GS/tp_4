from Fiche_signalitique import *
from Subject import *

class Student (Fiche_signalitique):

  """
  :version:
  :author: GOLIASSE Marina
  """

  """ ATTRIBUTES

  Mean  (private)

  Abs  (private)

  Start  (private)

  Last_name  (private)

  nouvel_attribut  (private)

  """

  def isClass(self):
    """
     ask if the student follow the class

    @return bool :
    @author
    """
    pass

  def addMean(self, value):
    """
    add mean on a subject for each student and calculate general mean
    @return float :
    @author
    """
    liste = []
    if self.first_mean is None:
      mean = Student(value)
      self.first_mean = mean
      liste.append(self.first_mean)

    else:
      mean = Student(value)
      mean.next = self.first_mean
      self.first_mean = mean
      liste.append(self.first_mean)


  def setLast(self):
    """
     Set last name of the student
    @return string :
    @author
    """
    pass


  def setStart(self):
    """
     Year the student entered university
    @return int :
    @author
    """
    pass



