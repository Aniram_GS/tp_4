from Fiche_signalitique import *
from Subject import *
from collections.abc import Iterable, Mapping
from functools import reduce
from Teachers import *
from Student import *

class Stud:

    def __init__(self, value):
        self.value = value
        self.next = None
        self.sum_tree = None

    def __str__(self):
        string = str(self.value)
        if self.next != None:
            string += "," + str(self.next)
        return string


class Subject:
    def __init__(self):
        self.first_mean = None
        self.first_class = None



    def add_Mean(self, value):
        """
        Add a mean for each subject and sum all student's means
        @return float :
        @author GOLIASSE Marina
        """
        liste = []
        if self.first_mean is None:
            mean = Stud(value)
            self.first_mean = mean
            liste.append(self.first_mean)

        else:
            mean = Stud(value)
            mean.next = self.first_mean
            self.first_mean = mean
            liste.append(self.first_mean)

    # def sum_tree(self):
    #     tree_sum = 0
    #     if self.first_mean:
    #         tree_sum += self.first_mean.sum_tree()
    #
    #     return tree_sum + self.value


    def add_Class(self, value):
        """
        add classroom for subject
    @return string :
    @author GOLIASSE Marina
    """
        liste = []
        if self.first_class is None:
            classroom = Stud(value)
            self.first_class = classroom
            liste.append(self.first_class)
            return liste
        else:
            classroom = Stud(value)
            classroom.next = self.first_mean
            self.first_mean = classroom
            liste.append(self.first_class)
            return liste


    def add_Sub(self):
        """
    @return string :
    @author
    """
        pass

    def __str__(self):
        if self.first_mean == None:
            return "liste vide"
        return str(self.first_mean)



sub = Subject()
sub.add_Mean(1)
sub.add_Mean(9)
sub.add_Class("S-101")
sub.add_Class("A-203")
#sub.addBT(9)

# sub.add_Mean(1)
print(sub)
#print(sub.sum_tree())

